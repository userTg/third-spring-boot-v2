package com.example.demo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Adresse;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.repository.AdresseRepository;
import com.example.demo.repository.PersonneRepository;
@RestController
@RequestMapping("/api/v1")
public class AdresseController {
    @Autowired
    private AdresseRepository  adresseRepository;
    @Autowired PersonneRepository personneRepository;
    @GetMapping("/personnes/{personneId}/adresses")
    public List<Adresse> getAdressesByPersonne(@PathVariable("personneId") Long personneId) {
        return adresseRepository.findAll();
    }
    @PostMapping("/personnes/{personneId}/adresses")
    public Adresse save(@PathVariable(value = "personneId") Long personneId,
        @Valid @RequestBody Adresse adresse) throws ResourceNotFoundException {
        return personneRepository.findById(personneId).map(personne -> {
            //course.setInstructor(instructor);
            return adresseRepository.save(adresse);
        }).orElseThrow(() -> new ResourceNotFoundException("personne not found"));
    }
    @PutMapping("/personnes/{personneId}/adresses/{adresseId}")
    public Adresse updateAdresse(@PathVariable(value = "personneId") Long personneId,
        @PathVariable(value = "adresseId") Long adresseId, @Valid @RequestBody Adresse adresseRequest)
    throws ResourceNotFoundException {
        if (!adresseRepository.existsById(personneId)) {
            throw new ResourceNotFoundException("Personne Id not found");
        }
        return adresseRepository.findById(adresseId).map(adresse -> {
            adresse.setRue(adresseRequest.getRue());
            return adresseRepository.save(adresse);
        }).orElseThrow(() -> new ResourceNotFoundException("adresse id not found"));
    }
    @DeleteMapping("/personnes/{personneId}/adresses/{adresseId}")
    public ResponseEntity <?> deleteAdresse(@PathVariable(value = "personneId") Long personneId,
        @PathVariable(value = "adresseId") Long adresseId) throws ResourceNotFoundException {
        return adresseRepository.findByIdAndPersonneNum(adresseId, personneId).map(adresse -> {
            adresseRepository.deleteById(adresseId);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Adresse not found with id " + adresseId + " and personneId " + personneId));
    }
}
