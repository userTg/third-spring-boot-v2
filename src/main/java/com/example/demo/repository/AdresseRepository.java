package com.example.demo.repository;





import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Adresse;




@Repository
public interface AdresseRepository extends JpaRepository<Adresse, Long>{

	Optional<Adresse> findByIdAndPersonneNum(Long id, Long personneNum);

	Optional<Adresse> findById(Long id);

	
	

	
	
	
	
}
