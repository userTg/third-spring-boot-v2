package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entities.Adresse;

import com.example.demo.entities.Personne;
import com.example.demo.repository.AdresseRepository;
import com.example.demo.repository.PersonneRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
class ThirdSpringBootV2AdresseRest {
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private PersonneRepository personneRepository;
	
	@Autowired
	private AdresseRepository adresseRepository;

	private Adresse adresseSavedInDb;

	@Test
	void testGetAdressesByPersonne() {
		Personne p1 = new Personne("Luke", "Lucky");
		
		Adresse ad1 = new Adresse();
		ad1.setCodePostal("12345");
		ad1.setRue("la Poisse");
		ad1.setVille("Nice");
		ad1.setPersonne(p1);
		
		adresseSavedInDb = entityManager.persist(ad1);
		Optional<Adresse> adresseFromDb = adresseRepository.findById(ad1.getId());
		assertThat(adresseFromDb.equals(adresseSavedInDb));
		

	}

	@Test
	void testSave() {
		Adresse pad1 = new Adresse();
		Personne p1 = new Personne("Luke", "Lucky");
		pad1.setCodePostal("12345");
		pad1.setRue("la Poisse");
		pad1.setVille("Nice");
		pad1.setPersonne(p1);
		Adresse aSavedInDb = entityManager.persist(pad1);
		Adresse aFromDb = adresseRepository.getOne(aSavedInDb.getId());
		assertEquals(aSavedInDb,aFromDb);
		assertThat(aFromDb.equals(aSavedInDb));
	}

	@Test
	void testUpdateAdresse() {
		Adresse ad1 = new Adresse();
		Personne p1 = new Personne("Luke", "Lucky");
		entityManager.persist(ad1);
		Adresse getFromDb = adresseRepository.getOne(ad1.getId());
		getFromDb.setRue("daube");
		assertThat(getFromDb.getRue().equals(ad1.getRue()));
	}

	@Test
	void testDeleteAdresse() {
		Personne p1 = new Personne("Dalton", "Joe");
		Personne p2 = new Personne("Luke", "Lucky");
		Adresse pad1 = new Adresse();
		Adresse pad2 = new Adresse();
		pad1.setPersonne(p1);
		pad2.setPersonne(p2);
		Adresse persist = entityManager.persist(pad1);
		entityManager.persist(pad2);
		entityManager.remove(persist);
		List<Adresse> allAdressesFromDb = adresseRepository.findAll();
		List<Adresse> adresseList = new ArrayList<>();
		for (Adresse adresse : allAdressesFromDb) {
			adresseList.add(adresse);
		}
		assertThat(adresseList.size()).isEqualTo(1);
	}

}
